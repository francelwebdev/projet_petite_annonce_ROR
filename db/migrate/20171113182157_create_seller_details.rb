class CreateSellerDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :seller_details do |t|
      t.string :name
      t.string :email
      t.integer :phone_number, limit: 8

      t.timestamps
    end
    add_index :seller_details, :email, unique: true
    add_index :seller_details, :phone_number, unique: true
  end
end
