class AddSellerTypeIdToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :seller_type_id, :integer
    add_index :ads, :seller_type_id
  end
end
