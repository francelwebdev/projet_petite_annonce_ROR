class CreateDoctors < ActiveRecord::Migration[5.1]
  def change
    create_table :doctors do |t|
      t.string :first_name
      t.string :second_name
      t.integer :phone_number, limit: 8
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
