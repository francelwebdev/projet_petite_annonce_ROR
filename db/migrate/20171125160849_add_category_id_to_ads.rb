class AddCategoryIdToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :category_id, :integer
    add_index :ads, :category_id
  end
end
