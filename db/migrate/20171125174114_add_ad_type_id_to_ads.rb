class AddAdTypeIdToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :ad_type_id, :integer
    add_index :ads, :ad_type_id
  end
end
