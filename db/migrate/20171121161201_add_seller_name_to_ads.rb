class AddSellerNameToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :seller_name, :string
  end
end
