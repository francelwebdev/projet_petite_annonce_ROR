class CreateAds < ActiveRecord::Migration[5.1]
  def change
    create_table :ads do |t|
      t.string :title
      t.text :description
      t.decimal :price, precision: 8, scale: 2
      t.references :sub_category, foreign_key: true

      t.timestamps
    end
  end
end
