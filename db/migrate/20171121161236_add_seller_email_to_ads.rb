class AddSellerEmailToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :seller_email, :string
  end
end
