class AddSellerPhoneNumberToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :seller_phone_number, :integer, limit: 8
  end
end
