class Ad < ApplicationRecord
	belongs_to :category
	belongs_to :ad_type
	belongs_to :seller_type

	# Validation

	validates :category_id, presence: { message: "VOus devez sélectionner une catégorie" }
	validates :title, presence: { message: "Le titre de votre annonce n'est pas renseigné" }
	validates :description, presence: { message: "La description de votre annonce n'est pas renseigné" }
	validates :price, presence: { message: "Le prix de votre annonce n'est pas renseigné" }
	validates :seller_name, presence: { message: "Votre nom n'est pas renseigné" }
	validates :seller_email, presence: { message: "Votre email n'est pas renseigné" }
	validates :seller_phone_number, presence: { message: "Votre numéro de téléphone n'est pas renseigné" }
	validates :seller_email, uniqueness: true
	validates :seller_phone_number, length: { is: 8 }, uniqueness: true
end
